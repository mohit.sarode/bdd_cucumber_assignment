package com.StepDefinitions;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(tags = "", features = "src/test/resources/Features/TestNGLogin.feature", glue = "com.StepDefinitions",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)


public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}
