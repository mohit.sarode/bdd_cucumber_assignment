 Feature: Login
 
 Scenario Outline: verify login functionality
 

Given user is on login page 
When user enters username "<username>" in email field
And user enter password  "<password>" in password field
Then click on login button
 
 
    Examples: 
    | username         | password |
    |mohits3@gmail.com | Mohit3 |
    |pushpak2@gmail.com| Pushpak2 |